Read Me
================

1. About

These Ansible roles allow you to install, from a deployment host, iRODS [4.2.0 - 4.2.8] on one or more CentOS 7.x / Ubuntu 18.04 x86_64 hosts. The iRODS role is compatible with both provider and consumer installation modes. PostgreSQL (stock versions from the distribution or newer ones) will be used to serve the iCAT catalogue. All firewalls should be stopped before attempting a deployment.

2. Ansible installation on CentOS

```bash
yum install epel-release # as root
yum update # as root
yum install python2-pip # as root
pip install --upgrade pip # as root
pip install ansible --user # first installation
pip install -U ansible --user # update
```

**N.B:** iRODS and postgresql roles are known to work with Ansible 2.10 

3. Gitlab repository

This Gitlab repository is comprised of two Ansible roles:

- irods
- postgresql

4. Fetch the roles on CentOS

```bash
yum install git # as root
mkdir -p ansible/{group_vars/{iRODS,all},host_vars,roles} ; cd ansible
$ git clone https://dci-gitlab.cines.fr/poc_irods/poc-irods.git roles
```

5. Work space on the deployment host

```
ansible
  |
  |_ irods.yml
  |_ irods_hosts
  |
  |_ group_vars
  |       |
  |       |_ iRODS
  |            |
  |            |_ main.yml
  |                             
  |_ host_vars
  |      |
  |      |_ myhost1.mydomain.tld
  |
  |_ roles
       |
       |_ irods
       |
       |_ postgresql
```

**N.B:** The provider / consumer installation mode is handled with the "irods_server_mode" variable in host_vars/myhost1.mydomain.tld file:

```
irods_server_mode: provider

```

6. Preparation of the irods.yml playbook

```
---

- name: Installation of PostgreSQL server
  hosts: irods_database
  roles:
    - postgresql
  become: true
  become_user: root
  become_method: sudo
  vars_files:
  - "group_vars/iRODS/main.yml"

- name: Installation of iRODS server
  hosts: irods_server
  roles:
    - irods
  become: true
  become_user: root
  become_method: sudo
  vars_files:
  - "group_vars/iRODS/main.yml"

...

```

7. Preparation of the irods_hosts inventory file

```
[irods_server]
myhost1.mydomain.tld

[irods_database]
myhost2.mydomain.tld

```
**N.B:** iRODS and PostgreSQL may also be installed on the same host.

8. Preparation of your vaulted variables

The values below are given as examples only, you can obviously modify them.

```bash
cd ansible/group_vars/iRODS/
ansible-vault create --vault-id irods@prompt main.yml
```
```
---

# Passwords

admin_password:
negotiation_key:
server_control_plane_key:
zone_key:
irods_unix_password: see https://docs.ansible.com/ansible/faq.html#how-do-i-generate-encrypted-passwords-for-the-user-module for details
database_password:

# Hosts

database_hostname: myhost2.mydomain.tld
provider_hostname: myhost1.mydomain.tld
provider_ip:

# Users and groups

postgresql_unix_group_name: postgres
postgresql_unix_user_name: postgres
irods_unix_group_name: irods
irods_unix_user_name: irods
irods_unix_group_id: 
irods_unix_user_id:
database_user: irodsdb

# iRODS

irods_version: from 4.2.0 to 4.2.8
use_local_mirror: true or false
path_to_data: /irods/data
path_to_icat: /irods/icat
irods_zone: TESTZONE
default_resource: demoResc
database_name: ICAT

# Misc

external_postgresql_version: from 9.4 to 13
use_external_epel: true or false
use_distribution_postgresql: true or false
cluster_name: only relevant for PostgreSQL 10 on Ubuntu 18.04
selinux_mode: enforcing or permissive
...
```

9. SSH keys

Ansible has only two dependencies: SSH and Python. For that reason you need to generate a pair of SSH keys (without password) on the deployment host and copy the public part of it to each target machines:

```
ssh-keygen -b 2018 -t rsa -C "Some comment" -f /home/user/.ssh/ansible_id_rsa
ssh-copy-id -i /home/user/.ssh/ansible_id_rsa.pub user@myhost1.mydomain.tld
ssh-copy-id -i /home/user/.ssh/ansible_id_rsa.pub user@myhost2.mydomain.tld

```
**N.B:** if SELinux is set to "enforcing", please don't forget to run "/usr/sbin/restorecon -r -v .ssh" on each target machine, else your public key won't work.

10. Run the playbook

```bash
cd ansible
ansible-playbook --key-file=/path/to/your/.ssh/ansible_id_rsa -i irods_hosts irods.yml --vault-id irods@prompt
```

The ansible-playbook command will then ask you to input your password in order to decrypt your vaulted variables.

**N.B:** in case you'd need a password to become another user using "sudo", just add the "-K" option to your ansible-playbook command line, and you'll be prompted for it.
